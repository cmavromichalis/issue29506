# Issue29506

This repo is to replicate Issue 29506 from here https://gitlab.com/gitlab-org/gitlab/-/issues/29506

## Problem

The problem as demonstrated from the master branch is when the .gitlab-ci.yml uses:
```
  only:
    - manual
```
We can not launch pipelines from the master branch. It gives the error as in this screen shot:
![alt text](error.png "Error Screeshot")

## Workaround

Using:
```
when: manual
```
in place of the `only: -manual` seems to fix it.